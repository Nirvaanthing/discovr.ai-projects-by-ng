import cv2
import pandas as pd 
import face_recognition
import numpy as np 


# Datasource Path
datasource_2_path = "./Datasource_2/"
datasource_1_path = "./Datasource_1/"

# Dataframe for both Datasource

datasource_1_df = pd.read_csv(datasource_1_path+"Datasource_1.csv")
datasource_2_df = pd.read_csv(datasource_2_path+"Datasource_2.csv")

print(datasource_1_df)
print(datasource_2_df)

df = pd.read_csv("matchedDatasource.csv")
i = 1

matchedDatasource = []
for imagesds1 in datasource_1_df['Image_Folder_Name']:
    picture_of_criminal = None
    picture_of_criminal = face_recognition.load_image_file("./"+imagesds1)
    criminalds1_face_encoding = None
    criminalds1_face_encoding = face_recognition.face_encodings(picture_of_criminal)[0]
    print(imagesds1)

    n = 0
    match = None
    for imagesds2 in datasource_2_df['Image_Folder_Name']:

        unknown_picture = face_recognition.load_image_file("./"+imagesds2)
        criminalds2_face_encoding = face_recognition.face_encodings(unknown_picture)[0]

        print(imagesds2)

        # Now we can see the two face encodings are of the same person with `compare_faces`!

        

        results = face_recognition.compare_faces([criminalds2_face_encoding],criminalds1_face_encoding,tolerance=0.5)
        print(results)
    
        if results[0] == True:
            print("Criminal Identified in other datasource.")
            print(imagesds2,imagesds1)
            match_found = True
            match = imagesds2
        else:
            print("Not a match")
        results = None



    if match_found:

        i = df['ID'].max()
        i = int(i)+ 1

        filename1 = imagesds1.split("/")
        filenameds1 = filename1[2]
        filenameds1a = filenameds1.split(".")
        ds1id = filenameds1a[0]
        
        filename2 = match.split("/")
        filename1ds2 = filename2[2]
        filename1ds2a = filename1ds2.split(".")
        ds2id = filename1ds2a[0]

     
        df.loc[i] = [i,str(ds1id)+","+str(ds2id),"Photo"]
        
        #df.append([])

        print(i,)
        match_found = None
    else:
        print(i,None,None)

      

print(df)
export_csv = df.to_csv (r'matchedDatasource.csv', index = None, header=True)