import cv2
import pandas as pd 
import face_recognition
import numpy as np 

ds1_path = "./Datasource_1/"
ds2_path = "./Datasource_2/Datasource_2.csv"

# Dataframe for both Datasource

datasource_1_df = pd.read_csv(ds1_path+"Datasource_1.csv")
datasource_2_df = pd.read_csv(ds2_path+"Datasource_2.csv")

df = pd.read_csv("matchedDatasource.csv")
i = 1

matchedDatasource = []

for ds1_images in datasource_1_df['Image_Folder_Name']:
    picture_of_criminal = None
    picture_of_criminal = face_recognition.load_image_file("./"+ds1_images)
    criminalds1_face_encoding = None
    criminalds1_face_encoding = face_recognition.face_encodings(picture_of_criminal)[0]
    match = None
    for imagesds2 in datasource_2_df['Image_Folder_Name']:

        unknown_picture = face_recognition.load_image_file("./"+imagesds2)
        criminalds2_face_encoding = face_recognition.face_encodings(unknown_picture)[0]

        print(imagesds2)

        # Now we can see the two face encodings are of the same person with `compare_faces`!

        

        results = face_recognition.compare_faces([criminalds2_face_encoding],criminalds1_face_encoding,tolerance=0.5)
        print(results)
    
        if results[0] == True:
            print("Criminal Identified in other datasource.")
            print(imagesds2,ds1_images)
            match_found = True
            match = imagesds2
        else:
            print("Not a match")
        results = None